package com.springRest.emsbackend.service.impl;

import com.springRest.emsbackend.dto.EmployeeDto;
import com.springRest.emsbackend.entity.Employee;
import com.springRest.emsbackend.mapper.EmployeeMapper;
import com.springRest.emsbackend.repository.EmployeeRepository;
import com.springRest.emsbackend.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;

    @Override
    public EmployeeDto createEmployee(EmployeeDto employeeDto) {
        Employee employee = EmployeeMapper.mapToEmployee(employeeDto);
        Employee savedEmployee=  employeeRepository.save(employee);
        return EmployeeMapper.mapToEmployeeDto(savedEmployee);
    }
}
